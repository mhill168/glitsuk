/**
 * Created by mhill168 on 08/07/15.
 */

(function () {

  'use strict';

  angular.module('glits').controller('StandController', ['$scope', '$rootScope', '$stateParams', 'glitzService', '$ionicPopup', 'localStorageService', StandController]);

  function StandController($scope, $rootScope, $stateParams, glitzService, $ionicPopup, localStorageService) {

    var vm = this;

    $scope.rightButtons = [{
      type: 'button-clear',
      content: 'Menu',
      tap: function (e) {
        $scope.openModal();
      }
    }];

    vm.data = {
      isLoading: true
    };

    $scope.loadingText = "Loading Stand";
    $scope.buttonTxt = "Add to Shortlist";


    vm.standId = Number($stateParams.id);
    glitzService.getFeedData().then(function (data) {

      vm.stand = _.chain(data.stands)
        .filter(findById)
        .map(function (item) {
          return {
            id: item.id,
            description: item.description,
            telephone: item.telephone,
            email: item.email,
            name: item.name,
            lead: item.lead,
            team: item.team,
            icon: item.icon,
            umbrella: item.umbrella,
            picture: item.picture,
            flag: false
          };
        })
        .value();

      vm.data.isLoading = false;
    });

    function findById(item) {
      return item.id === vm.standId;
    }

    var inStore = localStorageService.get('shortlists');
    $rootScope.shortlists = inStore || [];
    console.log($rootScope.shortlists);

    $scope.$watch('shortlists', function () {
      localStorageService.set('shortlists', $rootScope.shortlists);
    }, true);

    $scope.isAdded = false;
    $scope.flagged = false;
    $scope.$watch('isAdded', function () {
      $scope.buttonTxt = ($scope.isAdded ? 'Remove from Shortlist' : 'Add to Shortlist');
    });

    $scope.addStand = function (stand) {
      if($scope.isAdded) {
        var confirmAdd = $ionicPopup.confirm({
          title: 'Remove Shortlisted?',
          template: 'Are you sure you want to remove this stand??'
        });
        confirmAdd.then(function (res) {
          if(res) {
            $rootScope.shortlists.splice($rootScope.shortlists.indexOf(stand), 1);
            stand.flag = false;
            $scope.flagged = false;
            $scope.isAdded = ! $scope.isAdded;
          }
        });
      } else {
        $scope.isAdded = ! $scope.isAdded;
        $rootScope.shortlists.push(stand);
        stand.flag = true;
        $scope.flagged = "disabled";
        console.log($rootScope.shortlists);
      }
    };

  }

})();
