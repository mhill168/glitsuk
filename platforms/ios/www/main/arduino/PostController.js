/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    angular.module('glits').controller('PostController', ['$scope', 'messages', PostController]);

    function PostController($scope, messages) {

        var self = this;

        self.methods = [
            { text: "Forward", method: "go_forward" },
            { text: "Left", method: "go_left" },
            { text: "Reverse", method: "go_reverse" },
            { text: "Right", method: "go_right" }
        ];
        self.duration = 3000;

        self.addMessage = function(message){
            messages.add(message);
            self.duration = 3000;
        };

    }

})();

