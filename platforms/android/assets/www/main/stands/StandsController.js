/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    'use strict';

    angular.module('glits').controller('StandsController', ['$scope', 'glitzService', '$timeout', '$ionicScrollDelegate', StandsController]);

    function StandsController ($scope, glitzService, $timeout, $ionicScrollDelegate) {

        var vm = this;

        vm.navTitle = 'Stands';

        vm.rightButtons = [{
            type: 'button-clear',
            content: 'Menu',
            tap: function (e) {
                $scope.openModal();
            }
        }];

        vm.scrollTop = function () {
          $ionicScrollDelegate.scrollTop(true);
        };

        vm.getScrollPosition = function() {
            //monitor the scroll
            vm.moveData = $ionicScrollDelegate.getScrollPosition().top;
            if(vm.moveData>=250){
                $('.scrollTop').fadeIn();
            }else if(vm.moveData<250){
                $('.scrollTop').fadeOut();
            }
        };

        vm.change = function (option) {
          $ionicScrollDelegate.scrollTop();
          console.log(option);
        };

        //Contains the filter options
        $scope.filterOptions = {
            categories: [
                {id : 3, umbrella : 'End to End Effectiveness' },
                {id : 4, umbrella : 'Security & Compliance' },
                {id : 5, umbrella : 'Enterprise Architecture' },
                {id : 6, umbrella : 'Innovation' },
                {id : 7, umbrella : 'Culture' },
                {id : 8, umbrella : 'Agile IT' },
                {id : 9, umbrella : 'Multiple' }
            ]
        };

        //Mapped to the model to filter
        $scope.filterItem = {
            category: $scope.filterOptions.categories[0]
        };

        vm.data = {
            isLoading : true
        };

        glitzService.getFeedData().then(function (data) {
            if(data) {
                vm.stands = data.stands;
                vm.data.isLoading = false;
            }
        });

        vm.selectStand = function (standId) {
            console.log(standId);
            return standId === vm.standId;
        };
    }

})();
