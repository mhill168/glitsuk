/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    'use strict';

    angular.module('glits').controller('MarketplaceController', ['$scope', MarketplaceController]);

    function MarketplaceController ($scope) {

        $scope.navTitle = 'Marketplace';

        $scope.rightButtons = [{
            type: 'button-clear',
            content: 'Menu',
            tap: function (e) {
                $scope.openModal();
            }
        }];
    }

})();
