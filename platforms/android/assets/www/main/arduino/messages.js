/**
 * Created by mhill168 on 14/07/15.
 */

(function () {

    angular.module('glits').factory('messages', function(){

        var messages = {};

        messages.list = [];

        messages.add = function(message) {
            messages.list.push({
                id: messages.list.length + 1,
                method: message.method,
                duration: "(" + message.duration + ")"
            });
        };

        return messages;
    });

})();


