angular.module('glits', ['ionic', 'ui.bootstrap', 'uiGmapgoogle-maps', 'ngCordova', 'LocalStorageModule'])

  .run(function ($ionicPlatform, $rootScope, localStorageService, $state) {

    angular.element(document).on("click", function(e) {
      $rootScope.$broadcast("documentClicked", angular.element(e.target));
    });

    $ionicPlatform.ready(function () {

      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      var credentials = localStorageService.get('credentials');

      if(credentials) {
        $state.go('main.home');
      } else {
        $state.go('signin')
      }

    });
  })
  .config(['$httpProvider', 'localStorageServiceProvider', function($httpProvider, localStorageServiceProvider) {

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    localStorageServiceProvider.setPrefix('ls');

  }])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('signin', {
        url: "/sign-in",
        templateUrl: "main/signin/signin.html",
        controller: 'SigninController'
      })

      .state('main', {
        url: "/main",
        abstract: true,
        templateUrl: "main/menuContainer/menu.html",
        controller: 'MainController'
      })

      .state('main.home', {
        url: "/home",
        views: {
          'main': {
            templateUrl: "main/home/home.html",
            controller : 'HomeController'
          }
        }
      })

      .state('main.seminars', {
        url: "/seminars",
        views: {
          'main': {
            templateUrl: "main/seminars/seminars.html",
            controller: 'SeminarsController'
          }
        }
      })

      .state('main.marketplace', {
        url: "/marketplace",
        views: {
          'main': {
            templateUrl: "main/marketplace/marketplace.html",
            controller: 'MarketplaceController'
          }
        }
      })

      .state('main.stands', {
        url: "/marketplace/stands",
        views: {
          'main': {
            templateUrl: "main/stands/stands.html"
          }
        }
      })

      .state('main.stand', {
        url: "/marketplace/stands/:id",
        views: {
          'main': {
            templateUrl: "main/stands/stand.html"
          }
        }
      })

      .state('main.shortlist', {
        url: "/shortlist",
        views: {
          'main': {
            templateUrl: "main/shortlist/shortlist.html",
            controller: 'ShortlistController'
          }
        }
      })

      .state('main.competition', {
        url: "/marketplace/competition",
        views: {
          'main': {
            templateUrl: "main/competition/competition.html",
            controller: "CompetitionController"
          }
        }
      })

      .state('main.magic', {
        url: "/marketplace/magic-car",
        views: {
          'main': {
            templateUrl: "main/magicCar/magicCar.html",
            controller: "MagicController"
          }
        }
      })

      .state('main.drive', {
        url: "/marketplace/drive",
        abstract: true,
        views : {
          'main' : {
            templateUrl: "main/arduino/tabs.html",
            controller: 'ArduinoController'
          }
        }
      })

      .state('main.drive.method', {
        url: "/marketplace/method",
        views: {
          'method': {
            templateUrl: "main/arduino/method.html"
          }
        }
      })

      .state('main.drive.code', {
        url: "/marketplace/code",
        views: {
          'code': {
            templateUrl: "main/arduino/code.html"
          }
        }
      })


      .state('main.contact', {
        url: "/contact",
        views: {
          'main': {
            templateUrl: "main/contact/contact.html"
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/sign-in');
  });
