/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    'use strict';

    angular.module('glits').controller('CompetitionController', ['$scope', '$ionicLoading', CompetitionController]);

    function CompetitionController ($scope, $ionicLoading) {

        $scope.navTitle = 'Competition';

        var currentPlatform = ionic.Platform.platform();
        (currentPlatform === "ios" ?
            $scope.competitionLink = "itms-apps://itunes.apple.com/gb/app/aurasma/id432526396?mt=8" :
            (currentPlatform === "android" ?
                $scope.competitionLink = "market://play.google.com/store/apps/details?id=com.aurasma.aurasma" : "#")
        );
        $scope.currentPlatform = currentPlatform;
        console.log($scope.competitionLink);
    }

})();
