/**
 * Created by MHILL168 on 12/08/2015.
 */

(function () {

    'use strict';

    angular.module('glits').controller('HomeController', ['$scope', '$state', HomeController]);

    function HomeController($scope, $state) {

        $scope.navTitle = 'Home Page';

        $scope.rightButtons = [{
            type: 'button-clear',
            content: 'Menu',
            tap: function (e) {
                $scope.openModal();
            }
        }];
    }

})();
